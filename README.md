# microservices example

What it does:
- This is a small mock up of three different containers consisting of a front end, shopping cart, and products services. 
- The example containers are from the Techworld gitlab course and builds on the concepts introduced in the container deployment exercise.
- The front end it hosted on port 3000, products service on 3001, and the shopping cart on 3002. See screen shot for result.

Setup:
- Docker and shell runners on single ubuntu server.
- Reusable blocks of code using "extends".
- Dynamic versioning of images when pushed to container registry.
- Deployment of front end app that gets data from products and shopping cart containers. 

Sequence:
- Build 
  - Create reusable code block to:
  - Declare image name and image tag based on service (front end, product, or shopping cart).
  - Build docker image and push to gitlab container registry.
  - Utilize the code block to create images for front end, product, and shopping cart.
- Deploy 
  - Obtain image from container registry and host on target server.
  - Set permissions on SSH key.
  - Delclare variables for image name and image tag.
  - Copy docker-compose.yml file to target server.
  - Log into docker registry to obtain container image.
  - Run commands to host the container. 
  - Utilize the code block to deploy front end, product, and shopping cart.
